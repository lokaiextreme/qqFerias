import csv
import smtplib
import os
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE
from datetime import datetime
from email import encoders
from typing import Optional
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
import requests

app = FastAPI()

origins = [
    "http://localhost:8080"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

class FeriasRequest(BaseModel):
    status: str
    data_inicio: str
    data_fim: str
    data_solicitacao: str
    observacao: Optional[str] = None

class FeriasRequest2(BaseModel):
    funcionario: str

class EmailRequest(BaseModel):
    assunto: str
    mensagem: str
    to_email: str

class CsvRequest(BaseModel):
    to_email: str
    ativos: int
    ferias: int

class CsvRequestSolicitacoes(BaseModel):
    to_email: str
    pendentes: int
    recusadas: int
    aprovadas: int
class CsvRequestMes(BaseModel):
    to_email: str
    janeiro: int
    fevereiro: int
    março: int
    abril: int
    maio: int
    junho: int
    julho: int
    agosto: int
    setembro: int
    outubro: int
    novembro: int
    dezembro: int

@app.post("/enviar_email")
async def enviar_email(email_request: EmailRequest):

    smtp_server = '10.0.0.241'
    smtp_port = 25
    smtp_username = "qqtech-alunos@quero-quero.com.br"
    server = smtplib.SMTP(smtp_server, smtp_port)
    server.ehlo()

    assunto = email_request.assunto
    mensagem = email_request.mensagem
    to_email = email_request.to_email
    from_email = smtp_username
    message = f"Subject: {assunto}\n\n{mensagem}"

    try:
        server.sendmail(from_email, to_email, message)

    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Erro ao enviar o e-mail: {e}")
    
    finally:
        server.quit()
        return {"message": "E-mail enviado com sucesso!"}

@app.post("/send_notification")
async def send_notification(ferias_request: FeriasRequest):
    url = "https://graph.facebook.com/v4.0/me/messages"
    token = "DQVJzbi1raTJheGV6Ump4NE5RYUVUTTZAwNGx5dmdJOE00VTRDUHR6RjVxdTdYOHYtaTcxQW9pVHBhOVhiTW9BQkdrU3A3dUdpOU1EQ0ZAfeU1HaHloSGZAuVGFNQkctSlhMVjJHUHZAMcU82OG90dG5heE9uLW9YSTlOdExBWU8zVnNybm5vMU5kMEFoalBDRUZAvNlkyVG5PSlhqWTEtaWZAyazJ3di0yTTJ1aEdmTG5JMTRTd0tUWFlpb3ctSWFDNndpUWh3OXdn"
    headers = {
        'Authorization': f'Bearer {token}',
        'Content-Type': 'application/json'
    }
    message_text = (
        f"Olá, sua solicitação de férias foi respondida "
        f"Status: {ferias_request.status}, "
        f"Data Início: {ferias_request.data_inicio}, "
        f"Data Fim: {ferias_request.data_fim}, "
        f"Data Solicitação: {ferias_request.data_solicitacao}, "
        f"Observação: {ferias_request.observacao or 'Nenhuma'}, "
    )
    data = {
        "messaging_type": "UPDATE",
        "recipient": {
            "id": 100088981670914
        },
        "message": {
            "text": message_text
        }
    }

    try: 
        response = requests.post(url, headers=headers, json=data)
        print(response.json())
    except:
        return {"message": "Erro ao enviar mensagem"}

    return {"message": "Enviado com sucesso"}

@app.post("/send_notification2")
async def send_notification(ferias_request: FeriasRequest2):
    url = "https://graph.facebook.com/v4.0/me/messages"
    token = "DQVJzbi1raTJheGV6Ump4NE5RYUVUTTZAwNGx5dmdJOE00VTRDUHR6RjVxdTdYOHYtaTcxQW9pVHBhOVhiTW9BQkdrU3A3dUdpOU1EQ0ZAfeU1HaHloSGZAuVGFNQkctSlhMVjJHUHZAMcU82OG90dG5heE9uLW9YSTlOdExBWU8zVnNybm5vMU5kMEFoalBDRUZAvNlkyVG5PSlhqWTEtaWZAyazJ3di0yTTJ1aEdmTG5JMTRTd0tUWFlpb3ctSWFDNndpUWh3OXdn"
    headers = {
        'Authorization': f'Bearer {token}',
        'Content-Type': 'application/json'
    }
    message_text = (
        f"Olá, existe uma solicitação pendente feita por {ferias_request.funcionario} "
    )
    data = {
        "messaging_type": "UPDATE",
        "recipient": {
            "id": 100088981670914
        },
        "message": {
            "text": message_text
        }
    }

    try: 
        response = requests.post(url, headers=headers, json=data)
        print(response.json())
    except:
        return {"message": "Erro ao enviar mensagem"}

    return {"message": "Enviado com sucesso"}



@app.post("/enviar_relatorio")
async def enviar_relatorio(csv_request: CsvRequest):

    smtp_server = '10.0.0.241'
    smtp_port = 25
    smtp_username = "qqtech-alunos@quero-quero.com.br"
    server = smtplib.SMTP(smtp_server, smtp_port)
    server.ehlo()
    to_email = csv_request.to_email

    # Define o nome do arquivo CSV com base na data/hora atual
    timestamp = datetime.now().strftime("%Y%m%d-%H%M%S")
    filename = f"relatorio-funcionarios-{timestamp}.csv"

    # Define os cabeçalhos do arquivo CSV
    fieldnames = ["Ativos", "Férias"]

    numfuncativo = 2
    numfuncferias = 1
    with open(filename, mode="w", newline="") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({
            "Ativos": csv_request.ativos,
            "Férias": csv_request.ferias
        })

    msg = MIMEMultipart()
    msg['From'] = smtp_username
    msg['To'] = to_email
    msg['Subject'] = 'Relatório CSV'

    
    with open(filename, "rb") as f:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(f.read())
        encoders.encode_base64(part)
        part.add_header("Content-Disposition", f"attachment; filename={filename}")
        msg.attach(part)

    server.sendmail(smtp_username, to_email, msg.as_string())


    os.remove(filename)


    return {"message": "E-mail enviado com sucesso!"}


@app.post("/enviar_relatorio_solicitacoes")
async def enviar_relatorio(csv_request: CsvRequestSolicitacoes):

    smtp_server = '10.0.0.241'
    smtp_port = 25
    smtp_username = "qqtech-alunos@quero-quero.com.br"
    server = smtplib.SMTP(smtp_server, smtp_port)
    server.ehlo()
    to_email = csv_request.to_email

    timestamp = datetime.now().strftime("%Y%m%d-%H%M%S")
    filename = f"relatorio-funcionarios-{timestamp}.csv"

    fieldnames = ["Aprovadas", "Recusadas", "Pendentes"]

    numfuncativo = 2
    numfuncferias = 1
    with open(filename, mode="w", newline="") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({
            "Aprovadas": csv_request.aprovadas,
            "Recusadas": csv_request.recusadas,
            "Pendentes": csv_request.pendentes
        })

    msg = MIMEMultipart()
    msg['From'] = smtp_username
    msg['To'] = to_email
    msg['Subject'] = 'Relatório CSV'

    
    with open(filename, "rb") as f:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(f.read())
        encoders.encode_base64(part)
        part.add_header("Content-Disposition", f"attachment; filename={filename}")
        msg.attach(part)

    server.sendmail(smtp_username, to_email, msg.as_string())


    os.remove(filename)


    return {"message": "E-mail enviado com sucesso!"}



@app.post("/enviar_relatorio_mes")
async def enviar_relatorio(csv_request: CsvRequestMes):

    smtp_server = '10.0.0.241'
    smtp_port = 25
    smtp_username = "qqtech-alunos@quero-quero.com.br"
    server = smtplib.SMTP(smtp_server, smtp_port)
    server.ehlo()
    to_email = csv_request.to_email

    timestamp = datetime.now().strftime("%Y%m%d-%H%M%S")
    filename = f"relatorio-funcionarios-{timestamp}.csv"

    fieldnames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]

    numfuncativo = 2
    numfuncferias = 1
    with open(filename, mode="w", newline="") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({
            "Janeiro": csv_request.janeiro,
            "Fevereiro": csv_request.fevereiro,
            "Março": csv_request.março,
            "Abril": csv_request.abril,
            "Maio": csv_request.maio,
            "Junho": csv_request.junho,
            "Julho": csv_request.julho,
            "Agosto": csv_request.agosto,
            "Setembro": csv_request.setembro,
            "Outubro": csv_request.outubro,
            "Novembro": csv_request.novembro,
            "Dezembro": csv_request.dezembro,
        })

    msg = MIMEMultipart()
    msg['From'] = smtp_username
    msg['To'] = to_email
    msg['Subject'] = 'Relatório CSV'

    
    with open(filename, "rb") as f:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(f.read())
        encoders.encode_base64(part)
        part.add_header("Content-Disposition", f"attachment; filename={filename}")
        msg.attach(part)

    server.sendmail(smtp_username, to_email, msg.as_string())


    os.remove(filename)


    return {"message": "E-mail enviado com sucesso!"}